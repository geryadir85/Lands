﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lands.ViewModels
{
    public class MainViewModel
    {
        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }

        public LandsViewModel Lands
        {
            get;
            set;
        }
        #endregion


        #region Constructors
        public MainViewModel()
        {
            Instance = this; 
            this.Login = new LoginViewModel();
        }
        #endregion

        #region SingleTon

        public static MainViewModel Instance
        {
            get;
            set;
        }

        public static MainViewModel GetInstance()
        {
            if (Instance == null)
            {
                return new MainViewModel();
            }

            return Instance; 
        }
        #endregion
    }
}
